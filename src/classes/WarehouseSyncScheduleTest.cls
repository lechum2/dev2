@isTest
public class WarehouseSyncScheduleTest {
    @isTest
    public static void onScheduleExecute_shouldCalloutAndUpsertEquipment() {
        Test.setMock(HttpCalloutMock.class, new WarehouseCalloutServiceMock());

        Test.startTest();
        String jobId = System.schedule('testWarehouseSync', '0 0 * * * ?', new WarehouseSyncSchedule());

        CronTrigger ct = [
            SELECT Id, CronExpression, TimesTriggered, NextFireTime
            FROM CronTrigger
            WHERE id = :jobId
        ];

        System.assertEquals('0 0 * * * ?', ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);

        Test.stopTest();
    }
}
