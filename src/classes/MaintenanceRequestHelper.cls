public class MaintenanceRequestHelper {

    public static void updateWorkOrders(List<Case> newCases, Map<Id, Case> oldCasesMap){
        createRoutineCheckups(newCases, oldCasesMap);
    }

    public static void createRoutineCheckups(List<Case> newCases, Map<Id, Case> oldCasesMap) {
        List<Case> closedCheckups = new List<Case>();
        for(Case newCase : newCases) {
            Case oldCase = oldCasesMap.get(newCase.Id);
            if((newCase.Type == 'Repair'
                || newCase.Type == 'Routine Maintenance')
                    && newCase.Status == 'Closed'
                    && oldCase.Status != 'Closed'
            ) {
                closedCheckups.add(newCase);
            }
        }
        if(closedCheckups.isEmpty())
            return;

        createRoutineCheckups(closedCheckups);
    }

    private static void createRoutineCheckups(List<Case> cases) {
        List<Case> maintenanceRequests = getMaintenanceRequests(cases);
        List<Work_Part__c> allWorkParts = new List<Work_Part__c>();
        for(Case mr : maintenanceRequests) {
            allWorkParts.addAll(mr.Work_Parts__r);
        }

        Map<Id, Case> checkupsByOldId = new Map<Id, Case>();
        for(Case maintenanceRequest : maintenanceRequests) {
            checkupsByOldId.put(maintenanceRequest.Id, createNewCheckup(maintenanceRequest));
        }

        insert checkupsByOldId.values();
        List<Work_Part__c> workPartsToUpdate = new List<Work_Part__c>();
        for(Work_Part__c wp : allWorkParts) {
            workPartsToUpdate.add(new Work_Part__c(
                Id = wp.Id,
                Maintenance_Request__c = checkupsByOldId.get(wp.Maintenance_Request__c).Id
            ));
        }
        update workPartsToUpdate;
    }

    private static List<Case> getMaintenanceRequests(List<Case> cases) {
        return [
            SELECT Id, Date_Reported__c, Date_Due__c, Vehicle__c, Vehicle__r.Name, Equipment__c, Origin, (
                SELECT Id, Equipment__r.Maintenance_Cycle__c, Maintenance_Request__c
                FROM Work_Parts__r
            )
            FROM Case
            WHERE Id IN :cases
        ];
    }

    private static Case createNewCheckup(Case maintenanceRequest) {
        Case checkup = new Case(
            Vehicle__c = maintenanceRequest.Vehicle__c,
            Equipment__c = maintenanceRequest.Equipment__c,
            RecordTypeId = RecordTypeCache.getCaseRoutineMaintenanceRecordTypeId(),
            Subject = 'Routine Checkup of ' + maintenanceRequest.Vehicle__r.Name,
            Date_Reported__c = Date.today(),
            Type = 'Routine Maintenance',
            Origin = maintenanceRequest.Origin,
            Status = 'New'
        );

        checkup.Date_Due__c = getNearestPartDueDate(maintenanceRequest.Work_Parts__r);

        return checkup;
    }

    private static Date getNearestPartDueDate(List<Work_Part__c> parts) {
        if(parts.isEmpty())
            return date.today();

        Integer minMaintenanceCycleDays = parts[0].Equipment__r.Maintenance_Cycle__c.intValue();
        for(Work_Part__c part : parts) {
            if(part.Equipment__r.Maintenance_Cycle__c.intValue() < minMaintenanceCycleDays) {
                minMaintenanceCycleDays = part.Equipment__r.Maintenance_Cycle__c.intValue();
            }
        }
        return Date.today().addDays(minMaintenanceCycleDays);
    }
}