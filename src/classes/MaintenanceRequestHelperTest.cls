@isTest
public class MaintenanceRequestHelperTest {
    @testSetup
    static void makeData(){
        Vehicle__c vehicle = new Vehicle__c(
            Name = 'test'
        );
        insert vehicle;
        Product2 equipment1 = new Product2(
            ProductCode = 't1',
            Name = 'test1',
            Replacement_Part__c = true,
            Maintenance_Cycle__c = 10
        );
        insert equipment1;
        Product2 equipment2 = new Product2(
            ProductCode = 't2',
            Name = 'test2',
            Replacement_Part__c = true,
            Maintenance_Cycle__c = 1
        );
        insert equipment2;
        Case maintenanceRequest = new Case(
            RecordTypeId = RecordTypeCache.getCaseRoutineMaintenanceRecordTypeId(),
            Vehicle__c = vehicle.Id,
            Type = 'Routine Maintenance',
            Subject = 'apexTest'
        );
        insert maintenanceRequest;
        Work_Part__c part1 = new Work_Part__c(
            Equipment__c = equipment1.Id,
            Maintenance_Request__c = maintenanceRequest.Id
        );
        insert part1;
        Work_Part__c part2 = new Work_Part__c(
            Equipment__c = equipment2.Id,
            Maintenance_Request__c = maintenanceRequest.Id
        );
        insert part2;
    }

    @IsTest
    public static void onMaintenanceRequestUpdate_whenRequestIsClosed_CreateNewRequestWithSameVehicle() {
        Case maintenanceRequest = [SELECT Id, Vehicle__c FROM Case WHERE Subject = 'apexTest'].get(0);

        Test.startTest();
        maintenanceRequest.Status = 'Closed';
        update maintenanceRequest;
        Test.stopTest();

        List<Case> maintenanceRequests = [
            SELECT Id, Vehicle__c, Date_Due__c, Status
            FROM Case
        ];
        System.assertEquals(2, maintenanceRequests.size());
        Case newMaintenanceRequest;
        Case closedMaintenanceRequest;
        for(Case resultMaintenanceRequest : maintenanceRequests) {
            if(resultMaintenanceRequest.Status == 'Closed') {
                closedMaintenanceRequest = resultMaintenanceRequest;
            } else if(resultMaintenanceRequest.Status == 'New') {
                newMaintenanceRequest = resultMaintenanceRequest;
            }
        }
        System.assertEquals(Date.today().addDays(1), newMaintenanceRequest.Date_Due__c);
        System.assertEquals(maintenanceRequest.Vehicle__c, newMaintenanceRequest.Vehicle__c);
    }

    @isTest
    public static void onMaintenanceRequestUpdate_whenRequestIsNotClosed_NoNewMaintenanceShouldBeCreated() {
        Case maintenanceRequest = [SELECT Id FROM Case WHERE Subject = 'apexTest'].get(0);

        Test.startTest();
        maintenanceRequest.Status = 'Working';
        update maintenanceRequest;
        Test.stopTest();

        List<Case> maintenanceRequests = [
            SELECT Id, Vehicle__c, Date_Due__c, Status
            FROM Case
            WHERE Status = 'New'
        ];
        System.assertEquals(0, maintenanceRequests.size());
    }

    @isTest
    public static void onMaintenanceRequestUpdate_whenRequestWithoutPartsIsClosed_CreateNewRequestWithSameVehicleAndDueDateSetForToday() {
        Case maintenanceRequest = [SELECT Id, Vehicle__c FROM Case WHERE Subject = 'apexTest'].get(0);
        delete [SELECT Id FROM Work_Part__c WHERE Maintenance_Request__c = :maintenanceRequest.Id];

        Test.startTest();
        maintenanceRequest.Status = 'Closed';
        update maintenanceRequest;
        Test.stopTest();

        List<Case> maintenanceRequests = [
            SELECT Id, Vehicle__c, Date_Due__c, Status
            FROM Case
            WHERE Status = 'New'
        ];

        System.assertEquals(1, maintenanceRequests.size());
        System.assertEquals(Date.today(), maintenanceRequests.get(0).Date_Due__c);
    }
}