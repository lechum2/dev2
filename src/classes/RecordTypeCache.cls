public with sharing class RecordTypeCache {
    private static Map<String, Id> recordTypeIds;
    private static Map<Id, RecordType> recordTypesByIds;

    static {
        recordTypesByIds = new Map<Id, RecordType>();
        recordTypeIds = new Map<String, Id>();

        for(RecordType rt : [SELECT Id, Name, DeveloperName, SObjectType FROM RecordType]) {
            recordTypesByIds.put(rt.Id, rt);
            recordTypeIds.put(rt.SObjectType + '.' + rt.DeveloperName, rt.Id);
        }
    }

    public static String getRecordTypeDeveloperName(Id recordTypeId) {
        if(recordTypeId == null || !recordTypesByIds.containsKey(recordTypeId))
            return null;

        return recordTypesByIds.get(recordTypeId).DeveloperName;
    }

    public static Id getRecordTypeId(SObjectType type, String developerName) {
        String key = type + '.' + developerName;
        return recordTypeIds.get(key);
    }

    public static Id getCaseRepairRecordTypeId() {
        return getRecordTypeId(Case.SObjectType, 'Repair');
    }

    public static Id getCaseRoutineMaintenanceRecordTypeId() {
        return getRecordTypeId(Case.SObjectType, 'Routine_Maintenance');
    }
}