@isTest
private class WarehouseCalloutServiceTest {
    @isTest
    public static void onWarehouseSync_shouldCalloutAndUpsertEquipment() {
        Test.setMock(HttpCalloutMock.class, new WarehouseCalloutServiceMock());

        Test.startTest();
        WarehouseCalloutService.runWarehouseEquipmentSync();
        Test.stopTest();

        List<Product2> createdProducts = [
            SELECT Id
            FROM Product2
        ];

        System.assertEquals(2, createdProducts.size());
    }
}