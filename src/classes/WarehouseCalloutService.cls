public with sharing class WarehouseCalloutService {

    private static final String WAREHOUSE_URL = 'https://th-superbadge-apex.herokuapp.com/equipment';
    
    // complete this method to make the callout (using @future) to the
    // REST endpoint and update equipment on hand.
    @future(callout=true)
    public static void runWarehouseEquipmentSync(){
        getWarehouseEquipment();
    }

    private static void getWarehouseEquipment() {
        HttpRequest request = new HttpRequest();
        request.setEndpoint(WAREHOUSE_URL);
        request.setMethod('GET');
        Http http = new Http();
        HttpResponse response = http.send(request);
        String body = response.getBody();
        body = body.replace('_id', 'identity');
        List<ExternalEquipment> externalEquipmentList
            = (List<ExternalEquipment>)JSON.deserializeStrict(body, List<ExternalEquipment>.class);
        upsert mapToProducts(externalEquipmentList) Warehouse_SKU__c;
    }

    private static List<Product2> mapToProducts(List<ExternalEquipment> externalEquipmentList) {
        List<Product2> products = new List<Product2>();
        for(ExternalEquipment item : externalEquipmentList) {
            products.add(new Product2(
                Replacement_Part__c = true,
                Cost__c = item.cost,
                Current_Inventory__c = item.quantity,
                Lifespan_Months__c = item.lifespan,
                Maintenance_Cycle__c = item.maintenanceperiod,
                Warehouse_SKU__c = item.sku,
                Name = item.name,
                ProductCode = item.identity
            ));
        }
        return products;
    }

    public class ExternalEquipment {
        String identity;
        Boolean replacement;
        Integer quantity;
        String name;
        Integer maintenanceperiod;
        Integer lifespan;
        Decimal cost;
        String sku;
    }
}