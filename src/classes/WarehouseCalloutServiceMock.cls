@isTest
public class WarehouseCalloutServiceMock implements HttpCalloutMock {
    public HttpResponse respond(HttpRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setStatusCode(200);
        response.setBody(
             '['
            +'    {'
            +'        "_id": "test1",'
            +'        "replacement": false,'
            +'        "quantity": 5,'
            +'        "name": "test 1",'
            +'        "maintenanceperiod": 365,'
            +'        "lifespan": 120,'
            +'        "cost": 5000,'
            +'        "sku": "111"'
            +'    },'
            +'    {'
            +'        "_id": "test2",'
            +'        "replacement": true,'
            +'        "quantity": 183,'
            +'        "name": "test 2",'
            +'        "maintenanceperiod": 0,'
            +'        "lifespan": 0,'
            +'        "cost": 300,'
            +'        "sku": "222"'
            +'    }'
            +']'
        );
        return response;
    }
}