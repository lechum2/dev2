trigger MaintenanceRequest on Case (after update) {
    switch on Trigger.operationType {
        when AFTER_UPDATE {
            MaintenanceRequestHelper.updateWorkOrders(Trigger.new, Trigger.oldMap);
        }
    }
}