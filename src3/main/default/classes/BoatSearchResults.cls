public with sharing class BoatSearchResults {
    @AuraEnabled
    public static List<Boat__c> getBoats(string boatTypeId) {
        String query
            = 'SELECT '
                + Boat__c.Id.getDescribe().getName() + ', '
                + Boat__c.Picture__c.getDescribe().getName() + ', '
                + ' Contact__r.Name'
            + ' FROM Boat__c';
        if(String.isNotBlank(boatTypeId)){
            query += ' WHERE BoatType__c = \'' + boatTypeId + '\'';
        }
        return Database.query(query);
    }
}
