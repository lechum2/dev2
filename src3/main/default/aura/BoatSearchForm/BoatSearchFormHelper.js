({
    checkNewAvailability : function(component) {
        var createEvent = $A.get("e.force:createRecord");

        if (createEvent) {
            component.set("v.newAvailable", true);
        }
    },

    getAllBoatTypes : function(component) {
        var action = component.get("c.getBoatTypes");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.boatTypes", response.getReturnValue());
            } else {
                console.error(response);
            }
        });
        $A.enqueueAction(action);
    }
})
