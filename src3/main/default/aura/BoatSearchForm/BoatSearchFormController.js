({
    doInit : function(component, event, helper) {
        component.set("v.selectedBoatType", "");
        helper.getAllBoatTypes(component);
        helper.checkNewAvailability(component);
    },

    handleNewClick : function(component, event, helper) {
        var createRecordEvent = $A.get("e.force:createRecord");
        var selectedBoatType = component.get("v.selectedBoatType");
        createRecordEvent.setParams({
            "entityApiName": "Boat__c",
            "defaultFieldValues": {
                'BoatType__c': selectedBoatType ? selectedBoatType : null
            }
        });
        createRecordEvent.fire();
    },

    onFormSubmit : function(component, event, helper) {
        var formSubmitEvent = component.getEvent("formsubmit");
        var selectedBoatType = component.get("v.selectedBoatType");
        console.log("Selected boatTypeId: ", selectedBoatType);
        formSubmitEvent.setParam("formData", { "boatTypeId" : selectedBoatType });
        formSubmitEvent.fire();
    }
})
