({
    onSearch : function(component, event, helper) {
        var action = component.get("c.getBoats");
        var boatTypeId = component.get("v.boatTypeId");
        action.setParams({
            "boatTypeId" : boatTypeId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var boats = response.getReturnValue();
                console.log(boats);
                component.set("v.boats", boats);
            } else {
                console.error(response.getError());
            }
        });
        $A.enqueueAction(action);
    }
})
