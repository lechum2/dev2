({
    init : function(component, event, helper) {
        helper.onSearch(component, event, helper);
    },

    doSearch : function(component, event, helper) {
        var params = event.getParam('arguments');
        if (params) {
            var boatTypeId = params.boatTypeId;
            console.log("Method called with id: ", boatTypeId);
            component.set("v.boatTypeId", boatTypeId);
            helper.onSearch(component, event, helper);
        }
    },

    onBoatSelect : function(component, event, helper) {
        var boatId = event.getParam("boatId");
        component.set("v.selectedBoatId", boatId);
    }
})
