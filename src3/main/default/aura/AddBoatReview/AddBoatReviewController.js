({
    doInit : function(component, event, helper) {
        helper.onInit(component, event, helper);
    },
    onSave : function(component, event, helper) {
        var service = component.find("service");
        service.saveRecord(function(result) {
            if(result.state === "SUCCESS") {
                var toast = $A.get("e.force:showToast");
                if (toast) {
                    toast.setParams({
                        "title": "Success",
                        "message": "The record was saved."
                    });
                    toast.fire();
                } else {
                    alert("The record was saved.");
                }
                var boatReviewAdded = component.getEvent("BoatReviewAdded");
                boatReviewAdded.fire();
                helper.onInit(component, event, helper);
            } else {
                console.log(JSON.stringify(result));
            }
        })
    },
    onRecordUpdated : function(component, event, helper) {
        var toast = $A.get("e.force:showToast");
        if (toast) {
            toast.setParams({
                "title": "Success",
                "message": "The record was updated."
            });
            toast.fire();
        } else {
            alert("The record was updated.");
        }
    }
})
