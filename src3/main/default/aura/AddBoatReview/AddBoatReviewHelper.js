({
    onInit : function(component, event, helper) {
        var service = component.find("service");
        service.getNewRecord(
            "BoatReview__c",
            null,
            true,
            $A.getCallback(function() {
                var review = component.get("v.boatReview");
                var error = component.get("v.recordError");
                if(error || !review) {
                    console.log(error, review);
                    return;
                }
                review.Boat__c = component.get("v.boat").Id;
                component.set("v.boatReview", review);
            })
        );
    }
})
