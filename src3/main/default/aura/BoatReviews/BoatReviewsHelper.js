({
    onInit : function(component, event, helper) {
        var boat = component.get("v.boat");
        var apexAction = component.get("c.getAll");
        apexAction.setParams({
            "boatId": boat.Id
        });

        apexAction.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS") {
                component.set("v.boatReviews", response.getReturnValue());
            } else {
                console.log(JSON.stringify(response));
            }
        });
        $A.enqueueAction(apexAction);
    }
})
