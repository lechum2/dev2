({
    doInit : function(component, event, helper) {
        helper.onInit(component, event, helper);
    },

    onUserInfoClick : function(component, event, helper) {
        var userId = event.target.getAttribute("data-userid");
        var navigateEvent = $A.get("e.force:navigateToSObject");
        if (navigateEvent) {
            navigateEvent.setParams({
                "recordId": userId
            });
            navigateEvent.fire();
        }
    }
})
