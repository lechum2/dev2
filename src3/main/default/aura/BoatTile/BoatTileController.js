({
    onBoatClick : function(component, event, helper) {
        var boatSelectEvent = component.getEvent("boatselect");
        var selectedBoat = component.get("v.boat");
        boatSelectEvent.setParam("boatId", selectedBoat.Id);
        boatSelectEvent.fire();

        var boatSelectedEvent = $A.get("e.c:boatSelected");
        boatSelectedEvent.setParam("boat", selectedBoat);
        boatSelectedEvent.fire();

        var plotMarkerEvent = $A.get("e.c:plotMapMarker");
        plotMarkerEvent.setParam("lat", selectedBoat.Geolocation__c.latitude);
        plotMarkerEvent.setParam("long", selectedBoat.Geolocation__c.longitude);
        plotMarkerEvent.setParam("sObjectId", selectedBoat.Id);
        plotMarkerEvent.setParam("label", selectedBoat.Name);
        plotMarkerEvent.fire();
    }
})
