({
    onFormSubmit : function(component, event, helper) {
        var formData = event.getParam("formData");
        console.log("Caught event with data :", formData.boatTypeId);
        var boatSearchResults = component.find("boatSearchResults");
        boatSearchResults.search(formData.boatTypeId);
    }
})
