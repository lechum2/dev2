({
    doInit : function(component, event, helper) {
        var navigateToSObject = $A.get("e.force:navigateToSObject");

        if (navigateToSObject) {
            component.set("v.navigatePossible", true);
        }
    },

    onFullDetails : function(component, event, helper) {
        var navigateToSObject = $A.get("e.force:navigateToSObject");
        var recordId = component.get("v.boat").Id;
        navigateToSObject.setParams({
            "recordId": recordId,
            "slideDevName": "detail"
        });
        navigateToSObject.fire();
    }
})
