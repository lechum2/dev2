trigger ProjectTrigger on Project__c (after update) {
    switch on Trigger.operationType {
        when AFTER_UPDATE {
            BillingCalloutService.callBillingService(Trigger.new, Trigger.oldMap);
        }
    }
}