@RestResource(urlMapping='/project')
global with sharing class ProjectRESTService {

    @HttpPost
    global static String postProjectData(
        String ProjectRef,
        String ProjectName,
        String OpportunityId,
        Date StartDate,
        Date EndDate,
        Double Amount,
        String Status
    ) {
        Savepoint sp = Database.setSavepoint();
        try {
            upsertProject(ProjectRef, ProjectName, OpportunityId, StartDate, EndDate, Amount, Status);
        } catch (Exception ex) {
            Database.rollback(sp);
            return ex.getMessage();
        }
        return 'OK';
    }

    private static void upsertProject(
        String ProjectRef,
        String ProjectName,
        String OpportunityId,
        Date StartDate,
        Date EndDate,
        Double Amount,
        String Status
    ) {
        Project__c project = new Project__c(
            ProjectRef__c = ProjectRef,
            Name = ProjectName,
            Opportunity__c = OpportunityId,
            Start_Date__c = StartDate,
            End_Date__c = EndDate,
            Status__c = Status,
            Billable_Amount__c = Amount
        );

        upsert project Project__c.ProjectRef__c;

        Opportunity referencedOpp = new Opportunity(
            Id = OpportunityId,
            DeliveryInstallationStatus__c = 'In progress'
        );
        update referencedOpp;
    }
}