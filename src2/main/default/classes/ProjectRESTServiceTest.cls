@isTest
private class ProjectRESTServiceTest {
    @TestSetup
    static void makeData() {
        Account testAccount = new Account(
            Name = 'testAccount'
        );
        insert testAccount;

        insert new Opportunity(
            Name = 'testOpp',
            AccountId = testAccount.Id,
            CloseDate = Date.today(),
            Amount = 200,
            StageName = 'Prospecting',
            DeliveryInstallationStatus__c = 'Yet to begin'
        );
    }

    @isTest
    public static void onCallToProjectRestService_shouldCreateProjectAndUpdateOpportunity() {
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'testOpp'].get(0);

        Test.startTest();
        String result = ProjectRESTService.postProjectData(
            'testProjectRef',
            'testProject',
            opp.Id,
            Date.today(),
            Date.today(),
            200,
            'Running'
        );
        Test.stopTest();

        Project__c createdProject = [
            SELECT
                ProjectRef__c,
                Opportunity__r.DeliveryInstallationStatus__c
            FROM Project__c
            WHERE Opportunity__c = :opp.Id
        ].get(0);

        System.assertEquals('OK', result);
        System.assertEquals('testProjectRef', createdProject.ProjectRef__c);
        System.assertEquals('In progress', createdProject.Opportunity__r.DeliveryInstallationStatus__c);
    }

    @isTest
    public static void onCallToProjectRestServiceWithBadOppId_shouldNotCreateProjectAndNotUpdateOpportunity() {

        Test.startTest();
        String result = ProjectRESTService.postProjectData(
            'testProjectRef',
            'testProject',
            'opp.Id',
            Date.today(),
            Date.today(),
            200,
            'Running'
        );
        Test.stopTest();

        Opportunity opp = [SELECT Id, DeliveryInstallationStatus__c FROM Opportunity WHERE Name = 'testOpp'].get(0);

        System.assertNotEquals('OK', result);
        System.assertEquals('Yet to begin', opp.DeliveryInstallationStatus__c);
    }
}