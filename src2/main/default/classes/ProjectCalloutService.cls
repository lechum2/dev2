public class ProjectCalloutService {
    @InvocableMethod(label='PostOpportunityToPMS')
    public static void postOpportunityToPMS(List<Id> opportunityIds) {
        System.enqueueJob(new QueueablePMSCall(opportunityIds));
    }

    public class PmsOpportunity {
        String opportunityId;
        String opportunityName;
        String accountName;
        Date closeDate;
        Decimal amount;

        public PmsOpportunity(Opportunity opp) {
            this.opportunityId = opp.Id;
            this.opportunityName = opp.Name;
            this.accountName = opp.Account.Name;
            this.closeDate = opp.CloseDate;
            this.amount = opp.Amount;
        }
    }

    public class QueueablePMSCall implements Queueable, Database.AllowsCallouts {
        List<Id> opportunityIds;

        public QueueablePMSCall(List<Id> opportunityIds) {
            this.opportunityIds = opportunityIds;
        }

        public void execute(QueueableContext context) {
            ServiceTokens__c serviceToken = ServiceTokens__c.getValues('ProjectServiceToken');
            Id opportunityId = this.opportunityIds.get(0);
            Opportunity opportunity = [
                SELECT Id, Name, Account.Name, closeDate, Amount
                FROM Opportunity
                WHERE Id = :opportunityId
            ].get(0);
            PmsOpportunity pmsOpportunity = new PmsOpportunity(opportunity);

            HttpRequest request = new HttpRequest();
            request.setEndpoint('callout:ProjectService');
            request.setMethod('POST');
            request.setHeader('token', serviceToken.Token__c);
            request.setHeader('Content-Type', 'application/json');
            String serializedOpp = JSON.serialize(pmsOpportunity);
            request.setBody(serializedOpp);
            Http http = new Http();
            HTTPResponse response = http.send(request);
            if(response.getStatusCode() != 201) {
                throw new ProjectCalloutException(
                    String.format('Error when calling PMS: {0}', new List<Object> { response.getStatus() }));
            }
        }
    }

    public class ProjectCalloutException extends Exception {}
}