public class BillingCalloutService {
    public static void callBillingService(List<Project__c> updatedProjects, Map<Id, Project__c> oldProjects) {
        List<Id> billableProjects = getBillableProjects(updatedProjects, oldProjects);
        if(billableProjects.isEmpty()) return;
        sentProjectsForBilling(billableProjects);
    }

    private static List<Id> getBillableProjects(List<Project__c> updatedProjects, Map<Id, Project__c> oldProjects) {
        List<Id> billableProjects = new List<Id>();
        for(Project__c updatedProject : updatedProjects) {
            Project__c oldProject = oldProjects.get(updatedProject.Id);
            if(updatedProject.Status__c == 'Billable' && oldProject.Status__c != updatedProject.Status__c) {
                billableProjects.add(updatedProject.Id);
            }
        }
        return billableProjects;
    }

    @future(callout=true)
    private static void sentProjectsForBilling(List<Id> billableProjectIds) {
        List<Project__c> projects = [
            SELECT Id, Billable_Amount__c, ProjectRef__c
            FROM Project__c
            WHERE Id IN :billableProjectIds
        ];

        if(projects.isEmpty()) return;
        Project__c theProject = projects.get(0);
        
        BillingServiceProxy.project billingServiceProject = convertToBillingServiceProject(theProject);

        BillingServiceProxy.InvoicesPortSoap11 billingService = new BillingServiceProxy.InvoicesPortSoap11();
        String status = billingService.billProject(billingServiceProject);
        if(status == 'OK') {
            theProject.Status__c = 'Billed';
            update theProject;
        } else {
            throw new BillingServiceCalloutException('Callout to Billing Service failed with status: ' + status);
        }
    }

    private static BillingServiceProxy.project convertToBillingServiceProject(Project__c project) {
        BillingServiceProxy.project billingServiceProject = new BillingServiceProxy.project();
        billingServiceProject.billAmount = project.Billable_Amount__c;
        billingServiceProject.projectid = project.ProjectRef__c;

        ServiceCredentials__c serviceCredentials = ServiceCredentials__c.getValues('BillingServiceCredential');
        billingServiceProject.username = serviceCredentials.Username__c;
        billingServiceProject.password = serviceCredentials.Password__c;

        return billingServiceProject;
    }

    public class BillingServiceCalloutException extends Exception {}
}