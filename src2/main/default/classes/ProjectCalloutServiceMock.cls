public class ProjectCalloutServiceMock implements HttpCalloutMock {
    public HttpResponse respond(HttpRequest request) {
        HttpResponse response = new HttpResponse();
        response.setStatus('OK');
        response.setStatusCode(201);
        return response;
    }
}