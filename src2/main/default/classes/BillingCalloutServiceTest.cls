@isTest
private class BillingCalloutServiceTest {
    @TestSetup
    static void makeData() {
        insert new ServiceCredentials__c(
            Name = 'BillingServiceCredential',
            Username__c = 'testUser',
            Password__c = 'testPass'
        );

        Account testAccount = new Account(
            Name = 'testAccount'
        );
        insert testAccount;

        Opportunity opp = new Opportunity(
            Name = 'testOpp',
            AccountId = testAccount.Id,
            CloseDate = Date.today(),
            Amount = 200,
            StageName = 'Prospecting'
        );
        insert opp;

        insert new Project__c(
            Opportunity__c = opp.Id,
            Name = 'testProject',
            ProjectRef__c = 'testProject',
            Billable_Amount__c = 200
        );
    }

    @isTest
    public static void onProjectStatusChangeToBillable_callBillingService_changeStatusOnSuccess() {
        Test.setMock(WebServiceMock.class, new BillingCalloutServiceMock());
        Project__c project = [SELECT Id, Status__c FROM Project__c WHERE Name = 'testProject'].get(0);

        Test.startTest();
        project.Status__c = 'Billable';
        update project;
        Test.stopTest();

        Project__c resultProject = [SELECT Id, Status__c FROM Project__c WHERE Id = :project.Id].get(0);
        System.assertEquals('Billed', resultProject.Status__c);
    }
}