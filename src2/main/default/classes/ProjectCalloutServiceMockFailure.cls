public class ProjectCalloutServiceMockFailure implements HttpCalloutMock {
    public HttpResponse respond(HttpRequest request) {
        HttpResponse response = new HttpResponse();
        response.setStatus('Something went wrong');
        response.setStatusCode(500);
        return response;
    }
}