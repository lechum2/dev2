@isTest
private class ProjectCalloutServiceTest {
    @TestSetup
    static void makeData(){
        insert new ServiceTokens__c(
            Name = 'ProjectServiceToken',
            Token__c = 'testToken'
        );

        Account testAccount = new Account(
            Name = 'testAccount'
        );
        insert testAccount;

        insert new Opportunity(
            Name = 'testOpp',
            AccountId = testAccount.Id,
            CloseDate = Date.today(),
            Amount = 200,
            StageName = 'Prospecting'
        );
    }

    @isTest
    public static void whenSendingOpportunityToPMS_shouldThrowExceptionOnError() {
        Test.setMock(HttpCalloutMock.class, new ProjectCalloutServiceMockFailure());
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'testOpp'].get(0);

        ProjectCalloutService.ProjectCalloutException ex;
        try {
            Test.startTest();
            ProjectCalloutService.postOpportunityToPMS(new List<Id> { opp.Id });
            Test.stopTest();
        } catch (Exception e) {
            ex = (ProjectCalloutService.ProjectCalloutException)e;
        }
        
        System.assertEquals('Error when calling PMS: Something went wrong', ex.getMessage());
    }

    @isTest
    public static void whenSendingOpportunityToPMS_shouldNotThrowExceptionOnSuccess() {
        Test.setMock(HttpCalloutMock.class, new ProjectCalloutServiceMock());
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'testOpp'].get(0);

        ProjectCalloutService.ProjectCalloutException ex;
        try {
            Test.startTest();
            ProjectCalloutService.postOpportunityToPMS(new List<Id> { opp.Id });
            Test.stopTest();
        } catch (ProjectCalloutService.ProjectCalloutException e) {
            ex = e;
        }

        System.assertEquals(null, ex);
    }
}