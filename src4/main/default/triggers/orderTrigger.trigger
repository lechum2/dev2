trigger orderTrigger on Order (after update) {
    switch on Trigger.operationType {
        when AFTER_UPDATE {
            OrderHelper.afterUpdate(trigger.new, trigger.oldMap);
        }
    }
}