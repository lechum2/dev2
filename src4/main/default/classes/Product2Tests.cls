@IsTest
public class Product2Tests {

    /**
     * @name product2Extension_UnitTest
     * @description UnitTest for product2Extension
    **/
    private static TestMethod void Product2Extension_UnitTest(){
        Test.startTest();
        PageReference page = Page.Product2New;
        Test.setCurrentPage(page);
        Product2 prod = new Product2(Name = 'TestProduct', IsActive = true);
        ApexPages.StandardController controller = new ApexPages.StandardController(prod);
        
        Product2Extension ext = new Product2Extension(controller);
        System.assertEquals(Constants.DEFAULT_ROWS, ext.productsToInsert.size());
        ext.addRows();
        System.assertEquals(Constants.DEFAULT_ROWS * 2, ext.productsToInsert.size());

        for(Integer i=0;i<5;i++) {
            Product2Extension.ProductWrapper p = ext.productsToInsert[i];
            p.productRecord.Name = 'TestProduct' + i;
            p.productRecord.IsActive = true;
            p.productRecord.Initial_Inventory__c = 10;
            p.productRecord.Family = Constants.PRODUCT_FAMILY[Math.mod(i, Constants.PRODUCT_FAMILY.size())].getValue();
            p.priceBookEntryRecord.UnitPrice = 10;
        }
        ext.GetInventory();
        ext.save();
        Test.stopTest();

        List<Product2> resultProducts = [SELECT Id FROM Product2];
        System.assertEquals(5, resultProducts.size());
    }

    @IsTest
    private static void Product2Trigger_UnitTest() {
        String family = Constants.PRODUCT_FAMILY[0].getValue();
        Product2 prod = TestDataFactory.ConstructProducts(1).get(0);
        insert prod;
        insert TestDataFactory.ConstructCollaborationGroup();

        prod.Quantity_Ordered__c = prod.Initial_Inventory__c;
        Test.startTest();
        update prod;
        Test.stopTest();
    }
}