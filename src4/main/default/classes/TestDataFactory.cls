/**
 * @name TestDataFactory
 * @description Contains methods to construct and/or validate commonly used records
**/
public with sharing class TestDataFactory {

    /**
     * @name ConstructCollaborationGroup
     * @description
    **/
    public static CollaborationGroup ConstructCollaborationGroup(){
        //ToDo: Ensure this method returns a single Chatter CollaborationGroup
        //    whose Name starts with 'TEST' followed by the INVENTORY_ANNOUNCEMENTS constant
        //    and configured so anyone can join, see and post updates.
        return new CollaborationGroup(
            Name = 'TEST ' + Constants.INVENTORY_ANNOUNCEMENTS,
            CanHaveGuests = false,
            CollaborationType = 'Public',
            IsArchived = false,
            IsAutoArchiveDisabled = false
        );
    }

    /**
     * @name CreateProducts
     * @description Constructs a list of Product2 records for unit tests
    **/
    public static List<Product2> ConstructProducts(Integer cnt){
        //ToDo: Ensure this method returns a list, of size cnt, of uniquely named Product2 records
        //  with all the required fields populated
        //  and IsActive = true
        //  an Initial Inventory set to 10
        //  and iterating through the product family picklist values throughout the list.
        List<Product2> testProducts = new List<Product2>();
        for(Integer i=0; i<cnt; i++) {
            testProducts.add(new Product2(
                Name = 'TestProduct' + i,
                Family = Constants.PRODUCT_FAMILY[Math.mod(i, Constants.PRODUCT_FAMILY.size())].getValue(),
                Initial_Inventory__c = 10,
                IsActive = true
            ));
        }
        return testProducts;
    }

    /**
     * @name CreatePricebookEntries
     * @description Constructs a list of PricebookEntry records for unit tests
    **/
    public static List<PricebookEntry> ConstructPricebookEntries(List<Product2> prods){
        //ToDo: Ensure this method returns a corresponding list of PricebookEntries records
        //  related to the provided Products
        //  with all the required fields populated
        //  and IsActive = true
        //  and belonging to the standard Pricebook
        List<PricebookEntry> results = new List<PricebookEntry>();
        for(Product2 prod : prods) {
            results.add(new PricebookEntry(
                Product2Id = prod.Id,
                Pricebook2Id = Constants.STANDARD_PRICEBOOK_ID,
                IsActive = true,
                UnitPrice = 10
            ));
        }
        return results;
    }

    /**
     * @name CreateAccounts
     * @description Constructs a list of Account records for unit tests
    **/
    public static List<Account> ConstructAccounts(Integer cnt){
        //ToDo: Ensure this method returns a list of size cnt of uniquely named Account records
        //  with all of the required fields populated.
        List<Account> results = new List<Account>();
        for(Integer i=0; i<cnt; i++) {
            results.add(new Account(
                Name = 'TestAccount' + i
            ));
        }
        return results;
    }

    /**
     * @name CreateContacts
     * @description Constructs a list of Contacxt records for unit tests
    **/
    public static List<Contact> ConstructContacts(Integer cnt, List<Account> accts){
        //ToDo: Ensure this method returns a list, of size cnt, of uniquely named Contact records
        //  related to the provided Accounts
        //  with all of the required fields populated.
        List<Contact> results = new List<Contact>();
        for(Integer i=0; i<cnt; i++) {
            results.add(new Contact(
                LastName = 'TestContact' + i,
                AccountId = accts[Math.mod(i, accts.size())].Id
            ));
        }
        return results;
    }

    /**
     * @name CreateOrders
     * @description Constructs a list of Order records for unit tests
    **/
    public static List<Order> ConstructOrders(Integer cnt, List<Account> accts){
        //ToDo: Ensure this method returns a list of size cnt of uniquely named Order records
        //  related to the provided Accounts
        //  with all of the required fields populated.
        List<Order> results = new List<Order>();
        for(Integer i=0; i<cnt; i++) {
            results.add(new Order(
                Name = 'TestOrder' + i,
                AccountId = accts[Math.mod(i, accts.size())].Id,
                Pricebook2Id = Constants.STANDARD_PRICEBOOK_ID,
                EffectiveDate = Date.today(),
                Status = Constants.DRAFT_ORDER_STATUS
            ));
        }
        return results;
    }

    /**
     * @name CreateOrderItems
     * @description Constructs a list of OrderItem records for unit tests
    **/
    public static List<OrderItem> ConstructOrderItems(integer cnt, list<pricebookentry> pbes, list<order> ords){
        //ToDo: Ensure this method returns a list of size cnt of OrderItem records
        //  related to the provided Pricebook Entries
        //  and related to the provided Orders
        //  with all of the required fields populated.
        //  Hint: Use the DEFAULT_ROWS constant for Quantity as it will be used in the next challenge
        List<OrderItem> results = new List<OrderItem>();
        for(Integer i=0; i<cnt; i++) {
            results.add(new OrderItem(
                Quantity = Constants.DEFAULT_ROWS,
                OrderId = ords[Math.mod(i, ords.size())].Id,
                PricebookEntryId = pbes[Math.mod(i, pbes.size())].Id,
                UnitPrice = 10
            ));
        }
        return results;
    }

    /**
     * @name SetupTestData
     * @description Inserts accounts, contacts, Products, PricebookEntries, Orders, and OrderItems.
    **/
    public static void InsertTestData(Integer cnt){
        //ToDo: Ensure this method calls each of the construct methods
        //  and inserts the results for use as test data.
        insert ConstructCollaborationGroup();
        List<Product2> products = ConstructProducts(cnt);
        insert products;
        List<PricebookEntry> pbEntries = ConstructPricebookEntries(products);
        insert pbEntries;
        List<Account> accounts = ConstructAccounts(cnt);
        insert accounts;
        insert ConstructContacts(cnt, accounts);
        List<Order> orders = ConstructOrders(cnt, accounts);
        insert orders;
        insert ConstructOrderItems(cnt, pbEntries, orders);
    }

    public static void VerifyQuantityOrdered(Product2 originalProduct, Product2 updatedProduct, Integer qtyOrdered) {
        System.assertEquals(originalProduct.Quantity_Ordered__c + qtyOrdered, updatedProduct.Quantity_Ordered__c);
    }
}