@IsTest
public class OrderTests {
    @TestSetup
    public static void SetupTestData() {
        TestDataFactory.InsertTestData(1);
    }

    @IsTest
    private static void OrderUpdate_UnitTest() {
        Integer addedQuantity = 0;
        Product2 p = [SELECT Id, Quantity_Ordered__c FROM Product2 LIMIT 1];
        List<Order> orders = [
            SELECT Id, (
                SELECT Id, Quantity
                FROM OrderItems
            )
            FROM Order
            WHERE Status = : Constants.DRAFT_ORDER_STATUS
        ];
        for(Order o : orders) {
            o.Status = Constants.ACTIVATED_ORDER_STATUS;
            for(OrderItem oi : o.OrderItems) {
                addedQuantity += Integer.valueOf(oi.Quantity);
            }
        }

        Test.startTest();
        update orders;
        Test.stopTest();

        Product2 updatedProduct = [SELECT Id, Quantity_Ordered__c FROM Product2 WHERE Id =: p.Id LIMIT 1];

        TestDataFactory.VerifyQuantityOrdered(p, updatedProduct, addedQuantity);
    }

    @IsTest
    private static void OrderExtension_UnitTest() {
        Test.startTest();
        PageReference page = Page.OrderEdit;
        Test.setCurrentPage(page);
        Account a = [SELECT Id FROM Account LIMIT 1];
        Order o = new Order(
            AccountId = a.Id,
            Name = 'TestedOrder',
            EffectiveDate = Date.today(),
            Status = Constants.DRAFT_ORDER_STATUS
        );
        ApexPages.StandardController controller = new ApexPages.StandardController(o);
        OrderExtension ext = new OrderExtension(controller);
        System.assertEquals(1, ext.orderItemList.size());
        ext.orderItemList[0].Quantity = 10;
        ext.OnFieldChange();
        ext.Save();
        System.assertNotEquals(null, o.Id);
        ext.First();
        ext.Last();
        ext.Previous();
        ext.Next();
        System.assertEquals(1, ext.GetPageNumber());
        System.assertEquals(1, ext.GetTotalPages());
        System.assertEquals(false, ext.GetHasNext());
        System.assertEquals(false, ext.GetHasPrevious());
    }
}