/**
 * @name OrderExtension
 * @description This class is provided for you to facilitate the Super Badge
**/
public class OrderExtension {

    public Order orderRecord {get;set;}
    public List<OrderItem> orderItemList {get;set;}
    public String selectedFamily {get;set;}
    public List<chartHelper.chartData> pieData {get;set;}
    public Decimal total {get;set;}

    public Map<Id,OrderItem> orderItemMap;
    ApexPages.StandardSetController standardSetController;

    public OrderExtension(ApexPages.StandardController standardController){
        orderRecord = (Order)standardController.getRecord();
        orderItemMap = new Map<id,OrderItem>();
        if ( orderRecord.Id != null ){
            orderRecord = queryOrderRecord(orderRecord.Id);
            for(OrderItem item : orderRecord.OrderItems) {
                orderItemMap.put(item.Product2Id, item);
            }
        }
        selectedFamily = Constants.SELECT_ONE;
        createStandardSetController();
        populateVisible();
    }

    private void createStandardSetController() {
        String query = '';
        query += 'SELECT Product2Id, Product2.Name, Product2.Family, Product2.Quantity_Remaining__c, UnitPrice ';
        query += 'FROM PricebookEntry ';
        query += 'WHERE IsActive = TRUE';
        if(selectedFamily != Constants.SELECT_ONE) {
            query += ' AND Product2.Family = \'' + selectedFamily + '\'';
        }
        standardSetController = new Apexpages.StandardSetController(Database.getQueryLocator(query));
        standardSetController.setPageSize(Constants.DEFAULT_ROWS);
    }

    //ToDo: Implement your own method to populate orderItemList
    //  that you will call after pagination and/or family selection
    private void populateVisible() {
        orderItemList = new List<OrderItem>();
        for(SObject o : standardSetController.getRecords()) {
            PricebookEntry entry = (PricebookEntry)o;
            if(orderItemMap.containsKey(entry.Product2Id)) {
                orderItemList.add(orderItemMap.get(entry.Product2Id));
            } else {
                OrderItem item = new OrderItem(
                    PricebookEntryId = entry.id,
                    Product2Id = entry.Product2Id,
                    Product2 = entry.Product2,
                    UnitPrice = entry.UnitPrice,
                    Quantity = 0
                );
                orderItemList.add(item);
                orderItemMap.put(entry.Product2Id, item);
            }
        }
    }

    /**
     * @name OnFieldChange
     * @description
    **/
    public void OnFieldChange(){
        //ToDo: Implement logic to store the values changed on the page
        for(OrderItem item : orderItemList) {
            orderItemMap.put(item.Product2Id, item);
        }
        //      and populate pieData
        //      and populate total
        total = 0;
        pieData = new List<ChartHelper.ChartData>();
        for(OrderItem item : orderItemMap.values()) {
            if(item.Quantity == 0) continue;
            pieData.add(new ChartHelper.ChartData(item.Product2.Name, item.Quantity * item.UnitPrice));
            total += item.Quantity * item.UnitPrice;
        }

    }

    /**
     * @name SelectFamily
     * @description
    **/
    public void SelectFamily(){
        //ToDo: Implement logic to filter based on the selected product family
        createStandardSetController();
        populateVisible();
    }

    /**
     * @name Save
     * @description
    **/
    public void Save(){
        //ToDo: Implement logic to save the Order and populated OrderItems
        System.Savepoint sp = Database.setSavepoint();
        try {
            orderRecord.Pricebook2Id = Constants.STANDARD_PRICEBOOK_ID;
            upsert orderRecord;

            List<OrderItem> itemsToUpsert = new List<OrderItem>();
            List<OrderItem> itemsToDelete = new List<OrderItem>();
            for(OrderItem item : orderItemMap.values()) {
                if(item.Quantity > 0) {
                    if(item.OrderId == null) {
                        item.OrderId = orderRecord.Id;
                    }
                    itemsToUpsert.add(item);
                } else if (item.Id != null) {
                    itemsToDelete.add(new OrderItem(Id = item.Id));
                }
            }
            if(!itemsToUpsert.isEmpty()) upsert itemsToUpsert;
            if(!itemsToDelete.isEmpty()) delete itemsToDelete;
        } catch (Exception e) {
            Database.rollback(sp);
            System.debug(e.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Constants.ERROR_MESSAGE));
        }
    }


    /**
     * @name First
     * @description
    **/
    public void First(){
        standardSetController.first();
        populateVisible();
    }


    /**
     * @name Next
     * @description
    **/
    public void Next(){
        standardSetController.next();
        populateVisible();
    }


    /**
     * @name Previous
     * @description
    **/
    public void Previous(){
        standardSetController.previous();
        populateVisible();
    }

    /**
     * @name Last
     * @description
    **/
    public void Last(){
        standardSetController.last();
        populateVisible();
    }

    /**
     * @name GetHasPrevious
     * @description
    **/
    public Boolean GetHasPrevious(){
        return standardSetController.getHasPrevious();
    }

    /**
     * @name GetHasNext
     * @description
    **/
    public Boolean GetHasNext(){
        return standardSetController.getHasNext();
    }

    /**
     * @name GetTotalPages
     * @description
    **/
    public Integer GetTotalPages(){
        return (Integer)Math.ceil(standardSetController.getResultSize() / (Decimal)Constants.DEFAULT_ROWS);
    }

    /**
     * @name GetPageNumber
     * @description
    **/
    public Integer GetPageNumber(){
        return standardSetController.getPageNumber();
    }

    /**
     * @name GetFamilyOptions
     * @description
    **/
    public List<SelectOption> GetFamilyOptions(){
        List<SelectOption> result = new List<SelectOption>();
        result.add(new SelectOption(Constants.SELECT_ONE, Constants.SELECT_ONE));
        for(Schema.PicklistEntry entry : Constants.PRODUCT_FAMILY) {
            result.add(new SelectOption(entry.value, entry.label));
        }
        return result;
    }

    /**
     * @name QueryOrderRecord
     * @description
    **/
    public static Order QueryOrderRecord(Id orderId){
        return [
            SELECT Id, AccountId, EffectiveDate, Name, Status, Pricebook2Id,
                (
                    SELECT Id, OrderId, Quantity, UnitPrice, PricebookEntryId, Product2Id,
                         Product2.Name, Product2.Family, Product2.Quantity_Remaining__c
                    FROM OrderItems
                )
            FROM Order
            WHERE Id = :orderId
        ];
    }

}