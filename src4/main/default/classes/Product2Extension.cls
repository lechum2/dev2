public class Product2Extension {

    public List<ProductWrapper> productsToInsert {get;set;}

    public Product2Extension(ApexPages.StandardController controller) {
        productsToInsert = new List<ProductWrapper>();
        addRows();
    }

    public void AddRows(){
        for ( Integer i=0; i<Constants.DEFAULT_ROWS; i++ ){
            ProductWrapper record = new ProductWrapper();
            record.productRecord = new Product2();
            record.productRecord.Initial_Inventory__c = 0;
            record.pricebookEntryRecord = new PricebookEntry();
            record.pricebookEntryRecord.UnitPrice = 0.0;
            productsToInsert.add( record );
        }
    }

    public List<ChartHelper.ChartData> GetInventory(){
        return ChartHelper.GetInventory();
    }

    public PageReference Save(){
        Savepoint sp = Database.setSavepoint();
        try {
            List<Product2> products = new List<Product2>();
            List<PricebookEntry> pbEntries = new List<PricebookEntry>();
            for(ProductWrapper wrapper : productsToInsert) {
                if(String.isNotBlank(wrapper.productRecord.Name)
                    && wrapper.pricebookEntryRecord.UnitPrice != 0.0
                    && wrapper.productRecord.Family != Constants.SELECT_ONE
                ) {
                    pbEntries.add(wrapper.pricebookEntryRecord);
                    wrapper.pricebookEntryRecord.Pricebook2Id = Constants.STANDARD_PRICEBOOK_ID;
                    wrapper.pricebookEntryRecord.IsActive = true;
                    products.add(wrapper.productRecord);
                }
            }
            insert products;
            for(Integer i=0; i<products.size(); i++) {
                pbEntries[i].Product2Id = products[i].Id;
            }
            insert pbEntries;

            //If successful clear the list and display an informational message
            apexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO,productsToInsert.size()+' Inserted'));
            productsToInsert.clear();   //Do not remove
            addRows();  //Do not remove
        } catch (Exception e){
            Database.rollback(sp);
            apexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Constants.ERROR_MESSAGE));
        }
        return null;
    }

    public List<SelectOption> GetFamilyOptions() {
        List<SelectOption> result = new List<SelectOption>();
        result.add(new SelectOption(Constants.SELECT_ONE, Constants.SELECT_ONE));
        for(Schema.PicklistEntry entry : Constants.PRODUCT_FAMILY) {
            result.add(new SelectOption(entry.value, entry.label));
        }
        return result;
    }

    public class ProductWrapper {
        public Product2 productRecord {get;set;}
        public PricebookEntry pricebookEntryRecord {get;set;}
    }
}