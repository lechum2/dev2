public class OrderHelper {
    public static void afterUpdate(List<Order> newList, Map<Id, Order> oldMap){
        Set<Id> orderIds = new Set<Id>();
        for (Order newOrder : newList) {
            if (newOrder.Status == Constants.ACTIVATED_ORDER_STATUS
                && oldMap.get(newOrder.Id).Status != Constants.ACTIVATED_ORDER_STATUS) {
                orderIds.add(newOrder.Id);
            }
        }
        rollUpOrderItems(orderIds);
    }

    /**
     * @name RollUpOrderItems
     * @description Given a set of Activated Order ids, query the child Order Items and related Products to calculate Inventory levels
     * @param Set<Id> activatedOrderIds
     * @return void
    **/
    public static void rollUpOrderItems(Set<Id> activatedOrderIds){
        //ToDo: Declare a Map named "productMap" of Ids to Product2 records
        Map<Id, Product2> productMap = new Map<Id, Product2>();

        //ToDo: Loop through a query of OrderItems related to the activatedOrderIds
        List<OrderItem> orderItems = [
            SELECT Id, Product2Id, Product2.Quantity_Ordered__c, Quantity
            FROM OrderItem
            WHERE OrderId IN :activatedOrderIds
        ];
        for(OrderItem activatedOrderItem : orderItems) {
            //ToDo: Populate the map with the Id of the related Product2 as the key and Product2 record as the value
            productMap.put(activatedOrderItem.Product2Id, activatedOrderItem.Product2);
        }

        //ToDo: Loop through a query that aggregates the OrderItems related to the Products in the ProductMap keyset
        for(AggregateResult result : [
            SELECT Product2Id, SUM(Quantity)summed
            FROM OrderItem
            WHERE Product2Id IN : productMap.keySet()
            GROUP BY Product2Id
        ]) {
            productMap.get((Id)result.get('Product2Id')).Quantity_Ordered__c = Integer.valueOf(result.get('summed'));
        }

        //ToDo: Perform an update on the records in the productMap
        update productMap.values();
    }
}